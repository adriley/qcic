package require

import (
	"errors"
	"fmt"
	"strings"
	"testing"
)

type testingMock struct {
	fatalMessage string
	fatalCalled  bool
}

func (tm *testingMock) Fatal(args ...interface{}) {
	tm.fatalMessage = fmt.Sprint(args...)
	tm.fatalCalled = true
}

func (tm testingMock) requireFatalNotCalled(t *testing.T, message string) {
	if tm.fatalCalled {
		t.Fatalf("%v. Fatal called with message '%v'", message, tm.fatalMessage)
	}
}

func (tm testingMock) requireFatalCalled(t *testing.T, message string) {
	if !tm.fatalCalled {
		t.Fatalf("%v. Fatal was not called", message)
	}
}

func (tm testingMock) requireFatalCalledWithMessage(t *testing.T, message string, fatalMessage string) {
	tm.requireFatalCalled(t, message)
	if tm.fatalMessage != fatalMessage {
		t.Fatalf("%v. Wrong message. Got '%v'", message, tm.fatalMessage)
	}
}

func (tm testingMock) requireFatalCalledWithMessageStarting(t *testing.T, message string, fatalMessage string) {
	tm.requireFatalCalled(t, message)
	if !strings.HasPrefix(tm.fatalMessage, fatalMessage) {
		t.Fatalf("%v. Wrong message. Got '%v'", message, tm.fatalMessage)
	}
}

func Test_Equal_BoolEqual(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	Equal(&tm, true, true)
	tm.requireFatalNotCalled(t, "Equal failed when comparing true to true")
}

func Test_Equal_BoolUnequal(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	Equal(&tm, true, false)
	tm.requireFatalCalledWithMessage(t, "Equal failed when comparing true to false", "Expected: true; Actual: false")
}

func Test_Equal_IntEqual(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	Equal(&tm, 894, 894)
	tm.requireFatalNotCalled(t, "Equal failed when comparing 894 to 894")
}

func Test_Equal_IntUnequal(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	Equal(&tm, -50, 50)
	tm.requireFatalCalledWithMessage(t, "Equal failed when comparing -50 to 50", "Expected: -50; Actual: 50")
}

func Test_Equal_IntSliceEqual(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	Equal(&tm, []int{7, 8, 9}, []int{7, 8, 9})
	tm.requireFatalNotCalled(t, "Equal failed when comparing []int{7,8,9} to []int{7,8,9}")
}

func Test_Equal_IntSliceUnequal(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	Equal(&tm, []int{7, 8, 9}, []int{7, 8, 10})
	tm.requireFatalCalledWithMessage(t, "Equal failed when comparing []int{7, 8, 9} to []int{7, 8, 10}", "Expected: [7 8 9]; Actual: [7 8 10]")
}

func Test_True_WithTrueActualValue(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	True(&tm, true)
	tm.requireFatalNotCalled(t, "True failed when actual was true")
}

func Test_True_WithFalseActualValue(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	True(&tm, false)
	tm.requireFatalCalledWithMessage(t, "True failed when actual was false", "Expected: true; Actual: false")
}

func Test_False_WithTrueActualValue(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	False(&tm, true)
	tm.requireFatalCalledWithMessage(t, "False failed when actual was true", "Expected: false; Actual: true")
}

func Test_False_WithFalseActualValue(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	False(&tm, false)
	tm.requireFatalNotCalled(t, "False failed when actual was false")
}

func Test_Nil_NilPointer_True(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	var p *int
	Nil(&tm, p)
	tm.requireFatalNotCalled(t, "Nil failed when testing nil pointer")
}

func Test_Nil_NonNilPointer_False(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	n := 214
	p := &n
	Nil(&tm, p)
	tm.requireFatalCalledWithMessageStarting(t, "`Nil failed when testing non-nil pointer", "Expected: nil; Actual: 0x")
}

func Test_Nil_NilSlice_True(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	var s []int
	Nil(&tm, s)
	tm.requireFatalNotCalled(t, "Nil failed when testing nil slice")
}

func Test_Nil_EmptySlice_False(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	s := []int{}
	Nil(&tm, s)
	tm.requireFatalCalledWithMessage(t, "Nil failed when testing empty slice", "Expected: nil; Actual: []")
}

func Test_Nil_NonEmptySlice_False(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	s := []int{1, 2, 3}
	Nil(&tm, s)
	tm.requireFatalCalledWithMessage(t, "Nil failed when testing non-empty slice", "Expected: nil; Actual: [1 2 3]")
}

func Test_Nil_NilError_True(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	var err error
	Nil(&tm, err)
	tm.requireFatalNotCalled(t, "Nil failed when testing nil error")
}

func Test_Nil_NonNilError_False(t *testing.T) {
	tm := testingMock{fatalCalled: false}
	err := errors.New("error text")
	Nil(&tm, err)
	tm.requireFatalCalledWithMessage(t, "Nil failed when testing non-nil error", "Expected: nil; Actual: error text")
}

type testingExample struct {
	fatalMessage string
	fatalCalled  bool
}

func (tm *testingExample) Fatal(args ...interface{}) {
	fmt.Print(args...)
}

var t *testingExample

func methodExpectedToReturnTrueButReturnsFalse() bool {
	return false
}

func ExampleTrue_failure() {
	result := methodExpectedToReturnTrueButReturnsFalse()
	True(t, result)
	// Output: Expected: true; Actual: false
}
