package require

import (
	"fmt"
	"reflect"
)

// Testing implements a subset of the testing.T methods
type Testing interface {
	Fatal(args ...interface{})
}

// Equal calls t.Fatal if expected not equal to actual
func Equal(t Testing, expected, actual interface{}) {
	if reflect.DeepEqual(expected, actual) {
		return
	}
	callFatal(t, expected, actual)
}

// True calls t.Fatal if input is not true
func True(t Testing, actual bool) {
	Equal(t, true, actual)
}

// False calls t.Fatal if input is not false
func False(t Testing, actual bool) {
	Equal(t, false, actual)
}

// Nil calls t.Fatal if input is not nil
func Nil(t Testing, actual interface{}) {
	if actual == nil {
		return
	}
	switch reflect.TypeOf(actual).Kind() {
	case reflect.Ptr, reflect.Map, reflect.Array, reflect.Chan, reflect.Slice:
		if reflect.ValueOf(actual).IsNil() {
			return
		}
	}
	callFatal(t, "nil", actual)
}

func callFatal(t Testing, expected, actual interface{}) {
	message := fmt.Sprintf("Expected: %v; Actual: %v", expected, actual)
	t.Fatal(message)
}
